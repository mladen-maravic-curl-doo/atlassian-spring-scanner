package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;

/**
 * A component that should only be in JIRA
 */
@JiraComponent
public class JiraOnlyComponent {
}
