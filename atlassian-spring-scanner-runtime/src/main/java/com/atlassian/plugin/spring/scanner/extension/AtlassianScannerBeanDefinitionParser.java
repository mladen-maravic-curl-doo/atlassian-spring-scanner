package com.atlassian.plugin.spring.scanner.extension;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.parsing.CompositeComponentDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.beans.factory.xml.XmlReaderContext;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.util.ClassUtils;
import org.w3c.dom.Element;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static org.springframework.beans.factory.support.AbstractBeanDefinition.AUTOWIRE_CONSTRUCTOR;
import static org.springframework.beans.factory.xml.BeanDefinitionParserDelegate.AUTOWIRE_ATTRIBUTE;

/**
 * This class is responsible for handling the "parsing" of the scan-indexes element in the spring beans file.
 * Ultimately, this is what kicks off the index scanner and is the starting point for registering bean definitions
 */
public class AtlassianScannerBeanDefinitionParser implements BeanDefinitionParser {
    private static final String AUTO_IMPORTS_ATTRIBUTE = "auto-imports";
    private static final String PROFILE_ATTRIBUTE = "profile";
    public static final String JAVAX_INJECT_CLASSNAME = "javax.inject.Inject";

    private final Log log = LogFactory.getLog(getClass());

    @Override
    public BeanDefinition parse(final Element element, final ParserContext parserContext) {
        boolean autoImports = false;
        String profileName = null;
        Integer autowireDefault = null;
        if (element.hasAttribute(AUTO_IMPORTS_ATTRIBUTE)) {
            autoImports = Boolean.parseBoolean(element.getAttribute(AUTO_IMPORTS_ATTRIBUTE));
        }
        if (element.hasAttribute(PROFILE_ATTRIBUTE)) {
            profileName = element.getAttribute(PROFILE_ATTRIBUTE);
        }
        if (element.hasAttribute(AUTOWIRE_ATTRIBUTE)) {
            // Pass the AUTOWIRE_ATTRIBUTE to the delegate for parsing.
            autowireDefault = parserContext.getDelegate().getAutowireMode(element.getAttribute(AUTOWIRE_ATTRIBUTE));
        }

        // Actually scan for bean definitions and register them.

        final BundleContext bundleContext = getBundleContext();
        final ClassIndexBeanDefinitionScanner scanner = new ClassIndexBeanDefinitionScanner(
                parserContext.getReaderContext().getRegistry(), profileName, autowireDefault, bundleContext);
        final Set<BeanDefinitionHolder> beanDefinitions = scanner.doScan();

        registerComponents(parserContext.getReaderContext(), beanDefinitions, element, autoImports, profileName);

        return null;
    }

    /**
     * Takes the scanned bean definitions and adds them to a root component. Also adds in the post-processors required
     * to import/export OSGi services. Finally fires the component registered event with our root component.
     *
     * @param readerContext   the xml context
     * @param beanDefinitions the set of bean definitions
     * @param element         the xml element of definition
     * @param autoImports     whether auto imports apply
     * @param profileName     the name of the profile to read definitions from
     */
    protected void registerComponents(
            final XmlReaderContext readerContext,
            final Set<BeanDefinitionHolder> beanDefinitions,
            final Element element,
            final boolean autoImports,
            final String profileName) {
        final Object source = readerContext.extractSource(element);
        final CompositeComponentDefinition compositeDef = new CompositeComponentDefinition(element.getTagName(), source);

        //Add the beans we found to our root component
        for (final BeanDefinitionHolder beanDefHolder : beanDefinitions) {
            compositeDef.addNestedComponent(new BeanComponentDefinition(beanDefHolder));
        }

        //add our custom post-processors along with the standard @Autowired processor
        final Set<BeanDefinitionHolder> processorDefinitions = new LinkedHashSet<BeanDefinitionHolder>();
        processorDefinitions.addAll(AnnotationConfigUtils.registerAnnotationConfigProcessors(readerContext.getRegistry(), source));

        //Let's be nice and support javax.inject.Inject annotations
        final BeanDefinitionHolder javaxInject = getJavaxInjectPostProcessor(readerContext.getRegistry(), source);
        if (null != javaxInject) {
            processorDefinitions.add(javaxInject);
        }
        processorDefinitions.add(getComponentImportPostProcessor(readerContext.getRegistry(), source, autoImports, profileName));
        processorDefinitions.add(getServiceExportPostProcessor(readerContext.getRegistry(), source, profileName));
        processorDefinitions.add(getDevModeBeanInitialisationLoggerPostProcessor(readerContext.getRegistry(), source));

        for (final BeanDefinitionHolder processorDefinition : processorDefinitions) {
            compositeDef.addNestedComponent(new BeanComponentDefinition(processorDefinition));
        }

        readerContext.fireComponentRegistered(compositeDef);
    }

    private BundleContext getBundleContext() {
        return FrameworkUtil.getBundle(AtlassianScannerBeanDefinitionParser.class).getBundleContext();
    }

    private BeanDefinitionHolder getJavaxInjectPostProcessor(final BeanDefinitionRegistry registry, final Object source) {
        if (ClassUtils.isPresent(JAVAX_INJECT_CLASSNAME, getClass().getClassLoader())) {
            try {
                final Class injectClass = getClass().getClassLoader().loadClass(JAVAX_INJECT_CLASSNAME);
                final Map<String, Object> properties = new HashMap<String, Object>();
                properties.put("autowiredAnnotationType", injectClass);

                final RootBeanDefinition def = new RootBeanDefinition(AutowiredAnnotationBeanPostProcessor.class);
                def.setSource(source);
                def.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
                def.setPropertyValues(new MutablePropertyValues(properties));

                return registerBeanPostProcessor(registry, def, "javaxInjectBeanPostProcessor");
            } catch (final ClassNotFoundException e) {
                log.error("Unable to load class '" + JAVAX_INJECT_CLASSNAME + "' for javax component purposes.  Not sure how this is possible.  Skipping...");
            }
        }

        return null;
    }

    /**
     * Helper to convert a post-processor into a proper holder
     *
     * @param registry   the registry of bean defs
     * @param definition the root definition
     * @param beanName   the name of the bean to register
     * @return a bean def holder
     */
    private BeanDefinitionHolder registerBeanPostProcessor(
            final BeanDefinitionRegistry registry, final RootBeanDefinition definition, final String beanName) {
        definition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);

        registry.registerBeanDefinition(beanName, definition);
        return new BeanDefinitionHolder(definition, beanName);
    }

    private BeanDefinitionHolder getComponentImportPostProcessor(
            final BeanDefinitionRegistry registry, final Object source, final boolean autoImports, final String profileName) {
        // Note that we need a null capable collection here
        final Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("autoImports", autoImports);
        properties.put("profileName", profileName);

        final RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(ComponentImportBeanFactoryPostProcessor.class);
        rootBeanDefinition.setAutowireMode(AUTOWIRE_CONSTRUCTOR);
        rootBeanDefinition.setSource(source);
        rootBeanDefinition.setPropertyValues(new MutablePropertyValues(properties));

        return registerBeanPostProcessor(registry, rootBeanDefinition, "componentImportBeanFactoryPostProcessor");
    }

    private BeanDefinitionHolder getServiceExportPostProcessor(
            final BeanDefinitionRegistry registry, final Object source, final String profileName) {
        // Note that we need a null capable collection here
        final HashMap<String, Object> properties = new HashMap<String, Object>();
        properties.put("profileName", profileName);

        final RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(ServiceExporterBeanPostProcessor.class);
        rootBeanDefinition.setSource(source);
        rootBeanDefinition.setAutowireMode(AUTOWIRE_CONSTRUCTOR);
        rootBeanDefinition.setPropertyValues(new MutablePropertyValues(properties));

        return registerBeanPostProcessor(registry, rootBeanDefinition, "serviceExportBeanPostProcessor");
    }

    private BeanDefinitionHolder getDevModeBeanInitialisationLoggerPostProcessor(
            final BeanDefinitionRegistry registry, final Object source) {
        final RootBeanDefinition def = new RootBeanDefinition(DevModeBeanInitialisationLoggerBeanPostProcessor.class);
        def.setSource(source);
        def.setAutowireMode(AUTOWIRE_CONSTRUCTOR);

        return registerBeanPostProcessor(registry, def, "devModeBeanInitialisationLoggerBeanPostProcessor");
    }
}
