
You can run this test as either the byte code scanner or the javac scanner via profiles.

The test results should be identical.

mvn install -P -bytecode-scanner - removes byte code scanner and hence tests the javac code
mvn install -P -javac-scanner - removes javac scanner and hence tests the byte code scanner code

If you don't use a profile then both profiles will run and hence the byte code scanner will overwrite the output
of the javac scanner.

To start with a plugins 3.1 container which uses Gemini Blueprint instead of SpringDM as an OSGi wrapper you can run:

atlas-debug -PrunWithGemini
