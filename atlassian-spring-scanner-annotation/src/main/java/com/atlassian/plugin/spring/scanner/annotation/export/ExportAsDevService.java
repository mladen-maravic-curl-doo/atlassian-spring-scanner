package com.atlassian.plugin.spring.scanner.annotation.export;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Denotes a component to be exported as an OSGi service available to other bundles only in dev mode.
 */
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExportAsDevService {
    /**
     * the interfaces the service should be exported as.
     * if not specified, the interfaces will be calculated using reflection
     *
     * @return the list of interfaces to export
     */
    Class<?>[] value() default {};
}

