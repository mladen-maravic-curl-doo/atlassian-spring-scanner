package com.atlassian.plugin.spring.scanner.core.vfs;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import javax.annotation.processing.Filer;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.Properties;

/**
 */
public class FilerBasedVirtualFile implements VirtualFile {
    private final Filer filer;
    private final String fileName;

    public FilerBasedVirtualFile(final Filer filer, final String fileName) {
        this.filer = filer;
        this.fileName = StringUtils.replaceChars(fileName, '\\', '/'); // in case running on windows
    }

    @Override
    public Collection<String> readLines() throws IOException {
        FileObject fileObject = filer.getResource(StandardLocation.CLASS_OUTPUT, "", fileName);
        InputStream in = null;
        try {
            in = fileObject.openInputStream();
            return CommonIO.readLines(new InputStreamReader(in));
        } catch (FileNotFoundException fne) {
            return Collections.emptyList();
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    @Override
    public void writeLines(final Iterable<String> lines) throws IOException {
        FileObject fileObject = filer.createResource(StandardLocation.CLASS_OUTPUT, "", fileName);
        OutputStream out = null;
        try {
            out = fileObject.openOutputStream();
            CommonIO.writeLines(new OutputStreamWriter(out), lines);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    @Override
    public void writeProperties(final Properties properties, final String comment) throws IOException {
        FileObject fileObject = filer.createResource(StandardLocation.CLASS_OUTPUT, "", fileName);
        OutputStream out = null;
        try {
            out = fileObject.openOutputStream();
            CommonIO.writeProperties(new OutputStreamWriter(out), properties, comment);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }
}