package com.atlassian.plugin.spring.scanner.test.moduletype;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

import java.util.Date;

/**
 */
public class BasicModuleDescriptor extends AbstractModuleDescriptor<String> {
    public BasicModuleDescriptor(final ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public String getModule() {
        return new Date().toString();
    }
}
