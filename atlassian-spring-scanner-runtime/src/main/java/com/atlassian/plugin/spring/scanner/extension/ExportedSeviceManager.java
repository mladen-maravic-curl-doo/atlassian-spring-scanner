package com.atlassian.plugin.spring.scanner.extension;


import com.atlassian.plugin.spring.scanner.util.SpringDMUtil;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import java.util.Hashtable;
import java.util.Map;

/**
 * Utility class to encapsulate the registration/de-registration of OSGi services exported by public components
 */
public class ExportedSeviceManager {
    private final Hashtable<Integer, GenericOsgiServiceFactoryBean> exporters;

    public ExportedSeviceManager() {
        this.exporters = new Hashtable<Integer, GenericOsgiServiceFactoryBean>();
    }

    /**
     * Exports a component as an OSGi service for use by other bundles
     *
     * @param bundleContext the bundle context
     * @param bean          the bean to register
     * @param beanName      the name of that bean
     * @param serviceProps  the service properties
     * @param interfaces    the interface of that bean
     * @return the service registration handle
     * @throws Exception if things go badly
     */
    public ServiceRegistration registerService(final BundleContext bundleContext, final Object bean, final String beanName, final Map<String, Object> serviceProps, final Class<?>... interfaces)
            throws Exception {
        final GenericOsgiServiceFactoryBean osgiExporter = createExporter(bundleContext, bean, beanName, serviceProps, interfaces);

        final int hashCode = System.identityHashCode(bean);
        exporters.put(hashCode, osgiExporter);

        return osgiExporter.getObject();
    }

    /**
     * Query whether a bean was registered as a service with this exported service manager.
     *
     * @param bean the bean to query
     * @return true if bean was registered, false if not.
     */
    public boolean hasService(final Object bean) {
        final int hashCode = System.identityHashCode(bean);
        return exporters.containsKey(hashCode);
    }

    /**
     * De-registers an OSGi service
     *
     * @param bundleContext the bundle context
     * @param bean          the bean to un-register
     */
    @SuppressWarnings("UnusedParameters")
    public void unregisterService(final BundleContext bundleContext, final Object bean) {
        final int hashCode = System.identityHashCode(bean);
        final GenericOsgiServiceFactoryBean exporter = exporters.get(hashCode);
        if (null != exporter) {
            exporter.destroy();
            exporters.remove(hashCode);
        }
    }

    /**
     * creates the OsgiServiceFactoryBean used by spring when registering services
     */
    private GenericOsgiServiceFactoryBean createExporter(final BundleContext bundleContext, final Object bean, final String beanName, final Map<String, Object> serviceProps, final Class<?>... interfaces)
            throws Exception {
        final OsgiServiceFactoryBeanFactory osgiServiceFactoryBeanFactory = SpringDMUtil.getInstance().getOsgiServiceFactoryBeanFactory();
        return osgiServiceFactoryBeanFactory.createExporter(bundleContext, bean, beanName, serviceProps, interfaces);
    }
}
