package com.atlassian.plugin.spring.scanner.extension;

import com.atlassian.plugin.spring.scanner.util.CommonConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.util.ClassUtils;

import java.beans.Introspector;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static com.atlassian.plugin.spring.scanner.util.AnnotationIndexReader.getIndexFilesForProfiles;
import static com.atlassian.plugin.spring.scanner.util.AnnotationIndexReader.readAllIndexFilesForProduct;
import static com.atlassian.plugin.spring.scanner.util.AnnotationIndexReader.splitProfiles;
import static com.atlassian.plugin.spring.scanner.util.BeanDefinitionChecker.needToRegister;

/**
 * This class is responsible for reading the class index files and generating bean definitions from them. We assume all
 * of the proper type checks/visibility checks have been done by the annotation processors. This means that if a class
 * is listed in an index, it qualifies as a bean candidate.
 */
public class ClassIndexBeanDefinitionScanner {
    protected final Log log = LogFactory.getLog(getClass());
    private final BeanDefinitionRegistry registry;
    private final String profileName;
    /**
     * A constant for {@link AbstractBeanDefinition#setAutowireMode(int)} used for beans created from the index,
     * or null if the autowire mode should not be set.
     */
    private final Integer autowireDefault;
    private final BundleContext bundleContext;

    public ClassIndexBeanDefinitionScanner(
            final BeanDefinitionRegistry registry,
            final String profileName,
            final Integer autowireDefault,
            final BundleContext bundleContext) {
        this.registry = registry;
        this.profileName = profileName;
        this.autowireDefault = autowireDefault;
        this.bundleContext = bundleContext;
    }

    /**
     * Gets the map of beanName -> beanDefinition and returns a set of bean definition holders
     *
     * @return a set of bean def holders
     */
    protected Set<BeanDefinitionHolder> doScan() {

        final Set<BeanDefinitionHolder> beanDefinitions = new LinkedHashSet<BeanDefinitionHolder>();
        final Map<String, BeanDefinition> namesAndDefinitions = findCandidateComponents();

        for (final Map.Entry<String, BeanDefinition> nameAndDefinition : namesAndDefinitions.entrySet()) {

            if (needToRegister(nameAndDefinition.getKey(), nameAndDefinition.getValue(), registry)) {
                final BeanDefinitionHolder definitionHolder = new BeanDefinitionHolder(
                        nameAndDefinition.getValue(), nameAndDefinition.getKey());
                beanDefinitions.add(definitionHolder);
                registerBeanDefinition(definitionHolder, registry);
            }
        }
        return beanDefinitions;
    }

    /**
     * Reads the components from the index file(s) and generates a map of beanName -> beanDefinitions for them
     *
     * @return a Map of bean names to bean definitions
     */
    public Map<String, BeanDefinition> findCandidateComponents() {
        final Map<String, BeanDefinition> candidates = new HashMap<String, BeanDefinition>();

        final Set<String> beanTypeAndNames = new TreeSet<String>();

        final String[] profileNames = splitProfiles(profileName);
        final ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        for (final String fileToRead : getIndexFilesForProfiles(profileNames, CommonConstants.COMPONENT_KEY)) {
            beanTypeAndNames.addAll(readAllIndexFilesForProduct(fileToRead, contextClassLoader, bundleContext));
        }

        for (final String beanTypeAndName : beanTypeAndNames) {
            final String[] typeAndName = StringUtils.split(beanTypeAndName, "#");

            final String beanClassName = typeAndName[0];
            String beanName = "";

            if (typeAndName.length > 1) {
                beanName = typeAndName[1];
            }

            if (StringUtils.isBlank(beanName)) {
                beanName = Introspector.decapitalize(ClassUtils.getShortName(beanClassName));
            }

            if (log.isDebugEnabled()) {
                log.debug(String.format("Found candidate bean '%s' from class '%s'", beanName, beanClassName));
            }

            final BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(beanClassName);
            if (null != autowireDefault) {
                beanDefinitionBuilder.setAutowireMode(autowireDefault);
            }
            candidates.put(beanName, beanDefinitionBuilder.getBeanDefinition());
        }

        return candidates;
    }

    /**
     * copyPasta from spring-context:component-scan classes
     */
    protected void registerBeanDefinition(final BeanDefinitionHolder definitionHolder, final BeanDefinitionRegistry registry) {
        BeanDefinitionReaderUtils.registerBeanDefinition(definitionHolder, registry);
    }
}
