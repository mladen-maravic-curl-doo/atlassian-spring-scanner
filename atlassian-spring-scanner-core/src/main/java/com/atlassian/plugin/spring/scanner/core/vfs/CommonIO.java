package com.atlassian.plugin.spring.scanner.core.vfs;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

/**
 */
public class CommonIO {
    public static void writeLines(Writer writer, Iterable<String> lines) throws IOException {
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(writer);
            for (String line : lines) {
                out.write(line);
                out.write("\n");
            }
        } finally {
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(writer);
        }
    }

    public static Collection<String> readLines(Reader reader) throws IOException {
        List<String> lines = new ArrayList<String>();
        BufferedReader in = null;
        try {
            in = new BufferedReader(reader);
            String line;
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
        } finally {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(reader);
        }
        return lines;
    }

    public static void writeProperties(Writer writer, final Properties properties, final String comment)
            throws IOException {
        try {
            properties.store(writer, comment);
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }
}
