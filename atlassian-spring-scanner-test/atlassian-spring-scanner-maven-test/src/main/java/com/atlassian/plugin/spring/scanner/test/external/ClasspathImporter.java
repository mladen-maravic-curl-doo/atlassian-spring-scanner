package com.atlassian.plugin.spring.scanner.test.external;

import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponentComposite;

/**
 */
@Scanned
public class ClasspathImporter {
    @ClasspathComponent
    ExternalJarComponentComposite externalJarComponentComposite;


    /*

    We reply on the extra jar scanning here to find these guys.  Neat eh!

    @ClasspathComponent
    ExternalJarComponent1 externalJarComponent1;
    @ClasspathComponent
    ExternalJarComponent2 externalJarComponent2;
    */

}
