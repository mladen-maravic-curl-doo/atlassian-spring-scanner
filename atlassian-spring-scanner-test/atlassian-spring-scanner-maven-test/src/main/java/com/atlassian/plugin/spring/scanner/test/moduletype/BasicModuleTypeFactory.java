package com.atlassian.plugin.spring.scanner.test.moduletype;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 */
@ModuleType
@Component
@Profile("bootstrap")
public class BasicModuleTypeFactory extends SingleModuleDescriptorFactory<BasicModuleDescriptor> {
    @Autowired
    public BasicModuleTypeFactory(final HostContainer hostContainer) {
        super(hostContainer, "basic", BasicModuleDescriptor.class);
    }
}
