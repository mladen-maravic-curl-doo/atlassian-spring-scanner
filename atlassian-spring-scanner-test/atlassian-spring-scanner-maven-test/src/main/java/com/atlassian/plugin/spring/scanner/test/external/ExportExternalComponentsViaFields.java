package com.atlassian.plugin.spring.scanner.test.external;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import javax.inject.Named;

@Scanned
public class ExportExternalComponentsViaFields {
    public static class ExternalServiceViaField {
    }

    public static class ExternalDevServiceViaField {
    }

    @ExportAsService
    @Named
    ExternalServiceViaField externalServiceViaField;

    @ExportAsDevService
    @Named
    ExternalDevServiceViaField externalDevServiceViaField;
}
