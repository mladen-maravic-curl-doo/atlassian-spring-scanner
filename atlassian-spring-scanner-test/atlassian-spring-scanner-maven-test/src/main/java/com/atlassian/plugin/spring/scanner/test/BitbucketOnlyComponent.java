package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.plugin.spring.scanner.annotation.component.BitbucketComponent;

/**
 * A component that should only be in Bitbucket
 */
@BitbucketComponent
public class BitbucketOnlyComponent {
}
