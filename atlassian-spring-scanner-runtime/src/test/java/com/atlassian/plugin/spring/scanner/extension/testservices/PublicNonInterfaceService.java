package com.atlassian.plugin.spring.scanner.extension.testservices;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.springframework.stereotype.Component;

@Component
@ExportAsService
public class PublicNonInterfaceService {
    public void a() {
    }
}
