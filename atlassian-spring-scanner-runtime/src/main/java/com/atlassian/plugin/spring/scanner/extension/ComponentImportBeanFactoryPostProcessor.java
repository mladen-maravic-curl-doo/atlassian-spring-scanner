package com.atlassian.plugin.spring.scanner.extension;

import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.spring.scanner.util.CommonConstants;
import com.atlassian.plugin.spring.scanner.util.SpringDMUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

import java.net.URL;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import static com.atlassian.plugin.spring.scanner.util.AnnotationIndexReader.getIndexFilesForProfiles;
import static com.atlassian.plugin.spring.scanner.util.AnnotationIndexReader.readAllIndexFilesForProduct;
import static com.atlassian.plugin.spring.scanner.util.AnnotationIndexReader.readPropertiesFile;
import static com.atlassian.plugin.spring.scanner.util.AnnotationIndexReader.splitProfiles;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * This class is run after all of the bean definitions have been gathered for the current bundle. It looks for any of
 * the *Import annotations and registers the proper OSGi imports. This is a BeanFactoryPostProcessor because it needs to
 * run before the beans are created so that the services are available when spring wires up the beans.
 */
@SuppressWarnings("UnusedDeclaration")
public class ComponentImportBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    private final BundleContext bundleContext;
    private boolean autoImports;
    private String profileName;

    private final Log log = LogFactory.getLog(getClass());

    public ComponentImportBeanFactoryPostProcessor(final BundleContext bundleContext) {
        this.bundleContext = bundleContext;
        this.autoImports = false;
        this.profileName = null;
    }

    /**
     * Reads the component import index file(s) and registers the bean wrappers that represent OSGi import services
     *
     * @param beanFactory the bean factory
     * @throws BeansException if bad bean business happens
     */
    @Override
    public void postProcessBeanFactory(final ConfigurableListableBeanFactory beanFactory) throws BeansException {
        final BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;

        final Bundle bundle = bundleContext.getBundle();

        final String[] profileNames = splitProfiles(profileName);
        final Set<String> classNames = new TreeSet<String>();
        for (final String fileToRead : getIndexFilesForProfiles(profileNames, CommonConstants.COMPONENT_IMPORT_KEY)) {
            classNames.addAll(readAllIndexFilesForProduct(fileToRead, bundle));
        }
        for (final String className : classNames) {
            final String[] typeAndName = StringUtils.split(className, "#");
            final String beanType = typeAndName[0];
            final String beanName = (typeAndName.length > 1) ? typeAndName[1] : "";

            try {
                final Class beanClass = beanFactory.getBeanClassLoader().loadClass(beanType);
                registerComponentImportBean(registry, beanClass, beanName);
            } catch (final ClassNotFoundException e) {
                log.error("Unable to load class '" + beanType + "' for component importation purposes.  Skipping...");
            }
        }

        processMetaData(registry, profileNames);
    }

    private void processMetaData(final BeanDefinitionRegistry registry, final String[] profileNames) {
        for (final String fileToRead : getIndexFilesForProfiles(profileNames, CommonConstants.METADATA_FILE)) {
            final URL url = bundleContext.getBundle().getResource(fileToRead);
            processMetaDataProperties(registry, readPropertiesFile(url));
        }
    }

    private void processMetaDataProperties(final BeanDefinitionRegistry registry, final Properties properties) {
        //
        // we don't have any metadata properties support yet but we might one day.  So this code is left in
        // which I know is against the spirit of YAGNI but then again cliches are just that - cliche!
    }

    /**
     * Figures out the proper bean name for the service and registers it
     *
     * @param registry  the registry of beans
     * @param beanClass the class of the bean to import
     * @param beanName  the name of the bean to import
     */
    private void registerComponentImportBean(final BeanDefinitionRegistry registry, final Class beanClass, final String beanName) {
        String serviceBeanName = beanName;

        if ("".equals(serviceBeanName)) {
            serviceBeanName = StringUtils.uncapitalize(beanClass.getSimpleName());
        }

        registerBeanDefinition(registry, serviceBeanName, "(objectClass=" + beanClass.getName() + ")", beanClass);
    }

    /**
     * Creates an OsgiServiceProxyFactoryBean for the requested import type.
     *
     * @param registry   the bean registry
     * @param beanName   the name of the bean
     * @param filter     the OSGI filter to apply
     * @param interfaces the interface of the imported type
     */
    private void registerBeanDefinition(
            final BeanDefinitionRegistry registry, final String beanName, final String filter, final Class interfaces) {
        final Class osgiServiceProxyFactoryBeanClass = SpringDMUtil.getInstance().getOsgiServiceFactoryBeanFactory().getProxyClass();
        final BeanDefinitionBuilder builder = BeanDefinitionBuilder.rootBeanDefinition(osgiServiceProxyFactoryBeanClass);
        builder.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_CONSTRUCTOR);
        builder.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
        //builder.setLazyInit(true);

        if (isNotBlank(filter)) {
            builder.addPropertyValue("filter", filter);
        }

        builder.addPropertyValue("interfaces", new Class[]{interfaces});
        builder.addPropertyValue("beanClassLoader", OsgiPlugin.class.getClassLoader());

        final BeanDefinition newDefinition = builder.getBeanDefinition();

        registry.registerBeanDefinition(beanName, newDefinition);
    }

    public void setAutoImports(final boolean autoImports) {
        this.autoImports = autoImports;
    }

    public void setProfileName(final String profileName) {
        this.profileName = profileName;
    }
}
