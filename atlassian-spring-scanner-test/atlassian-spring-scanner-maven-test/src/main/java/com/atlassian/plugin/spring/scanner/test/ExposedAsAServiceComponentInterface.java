package com.atlassian.plugin.spring.scanner.test;

public interface ExposedAsAServiceComponentInterface {
    void doStuff();
}
